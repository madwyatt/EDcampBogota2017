@extends('layouts.master')

@section('title')
    Error 404
@endsection

@section('header')
    @include('partials.header')
    @include('partials.user_menu')
@endsection

@section('content')
    <main class="Errors  u-afterFixed">
        <div class="Errors-container">
            <h2>404</h2>
            <p>Parece que te has perdido</p>
            <p>No te preocupes, sólo sigue la flecha</p>
            <a href="./" class="u-button u-bg-success u-white"><i class="fa fa-arrow-right u-white"></i></a>
        </div>
    </main>
@endsection
