export function navigation () {
  const d = document,
    w = window,
    panel = d.querySelector('.Panel'),
    panelBtn = d.querySelector('.Panel-button'),
    mq = w.matchMedia('(min-width: 64em)'),
    hamburger = d.querySelector('.hamburger')

  function closePanel (mq) {
    if (mq.matches) {
      panel.classList.remove('is-active')
      hamburger.classList.remove('is-active')
      d.body.classList.remove('is-active')

      d.addEventListener('mouseover', e => {
        if ( e.target.classList.contains('MyHistory') || e.target.classList.contains('ProfileMenu-link') || e.target.localName === 'b' ) {
          if (mq.matches) {
            panel.style.opacity = 1
            panel.style.display = 'block'
          }
        }
      }, true)

      d.addEventListener('mouseout', e => {
        if ( e.target.classList.contains('MyHistory') || e.target.classList.contains('ProfileMenu-link') || e.target.localName === 'b' ) {
          if (mq.matches) {
            panel.style.opacity = 0
            panel.style.display = 'none'
          }
        }
      }, true)
    } else{
      d.removeEventListener('mouseover', e => {
        if ( e.target.classList.contains('MyHistory') || e.target.classList.contains('ProfileMenu-link') || e.target.localName === 'b' ) {
          if (mq.matches) {
            panel.style.opacity = 1
            panel.style.display = 'block'
          }
        } else {
          return false;
        }
      }, false)

      d.removeEventListener('mouseout', e => {
        if ( e.target.classList.contains('MyHistory') || e.target.classList.contains('ProfileMenu-link') || e.target.localName === 'b' ) {
          if (mq.matches) {
            panel.style.opacity = 0
            panel.style.display = 'none'
          } else {
            return false;
          }
        }
      }, false)
    }
  }

  d.addEventListener('click', e => {
    if ( e.target.classList.contains('hamburger') || e.target.classList.contains('hamburger-inner') || e.target.classList.contains('hamburger-box') ) {
      e.preventDefault()
      panel.classList.toggle('is-active')
      hamburger.classList.toggle('is-active')
      d.body.classList.toggle('is-active')
    }
  }, true)

  mq.addListener(closePanel)
  closePanel(mq)
}
